# Carousel Component

A responsive draggable 5-card carousel component for Angular. See a [demo](carousel.vanhattum.xyz) here.

## Usage

To use this component:

### Install via your favourite package manager

`npm install @jasonvh/carousel` or `yarn add @jasonvh/carousel`.

### Import the module

```
// ... other imports
import { CarouselModule } from '@jasonvh/carousel';

@NgModule({
  imports: [
    // ... other modules
    CarouselModule
  ],
})
export class AppModule { }
```

### Use the component in your template

app.component.html:

```
<lib-carousel [items]="items"></lib-carousel>
```

app.component.ts:

```
public items = [
  { title: 'Mobile Internet', image: 'bench.png' },
  { title: 'Home Internet', image: 'glide.png' },
  { title: 'Get a device', image: 'bike.png' },
  { title: 'Add a Phone-line', image: 'gym-wall.png' },
  { title: 'Upgrade', image: 'snowmobile.png' },
];
```

Notes:

1. Images are fetched from `/assets/<image>`.
2. **There must be 5 items**.

## Known issues

1. The carousel leaves some space on the right in the smallest breakpoint (1080p). This isn't a hard issue and can be fixed, it just requires significant restructuring of the HTML and CSS in the component.
