import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';

interface Item {
  title: string;
  image: string;
}
interface Card {
  item: Item;
  index: number;
  id: number;
}
@Component({
  selector: 'lib-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit, AfterViewInit {
  @Input('items') items: Item[];
  @ViewChildren('card') cardElements!: QueryList<ElementRef>;

  public cards: Card[] = [];

  public isDragging = false;
  public isMoving = false;
  public allowTransition = true;

  public startX: number = 0;
  public currentX: number = 0;

  public cardWidth: number = 0;

  public ngOnInit(): void {
    // Initialise the cards array with a single item so that we can calculate the width of a card
    this.cards = [
      { item: this.items[this.items.length - 1], index: -1, id: 0 },
    ];
  }

  /*
   * Resize listener
   * Recalculate the width of a card when the window is resized
   */
  @HostListener('window:resize', ['$event'])
  public onResize() {
    this.calculateCardWidth();
    this.draw();
  }

  public ngAfterViewInit(): void {
    this.calculateCardWidth();

    // Fully initialise the list of cards,
    // including placeholders at the beginning and end of the array
    this.cards = [
      // Beginning placeholder
      { item: this.items[this.items.length - 1], index: 0, id: 0 },

      // Cards from items
      ...this.items.map((item, index) => ({
        item,
        index: index + 1,
        id: index + 1,
      })),

      // Final placeholder
      {
        item: this.items[0],
        index: this.items.length + 1,
        id: this.items.length + 1,
      },
    ];
  }

  /**
   * Calculcate the transformation of a card, based on it's index
   * and the calculated card width
   * @param index The index of the card
   */
  public getTransform(index: number) {
    let transform = 0;

    transform = (index - 1) * this.cardWidth;

    // Shift the center card up a bit
    if (this.isCenter(index)) {
      transform += this.cardWidth / 2;
    }

    // Shift all cards on the right of the center card
    if (index > 3.5) {
      transform += this.cardWidth;
    }

    return transform;
  }

  /**
   * Calculate the width of the card
   * This is only done upon initialisation and window redraw,
   * for performance reasons
   */
  public calculateCardWidth() {
    const cardRect = this.cardElements.first.nativeElement.getBoundingClientRect();
    this.cardWidth = cardRect.width;
  }

  /**
   * Calculate any performance-heavy measurements
   * (mostly drag-related calculations)
   */
  public draw() {
    requestAnimationFrame(() => {
      const delta = (this.currentX - this.startX) / this.cardWidth;
      this.startX = this.currentX;

      this.cards.forEach((c) => {
        c.index = (c.index + 7 + delta) % 7;
      });

      if (this.isDragging) {
        // Continue redrawing until mouse is released
        this.draw();
      }
    });
  }

  /**
   * Move the cards left or right
   * @param direction The direction to move in: -1 for right, 1 for left
   */
  public moveCards(direction: number) {
    if (this.isMoving || this.isDragging) {
      // We don't move cards if we're currently dragging or cards are moving
      return;
    }

    this.isMoving = true;

    // Move cards, update indices and update placeholders
    if (direction == 1) {
      const last = this.last;
      this.first.item = this.secondLast.item;
      this.cards.forEach((c) => c.index++);
      last.index = 0;
    } else {
      const first = this.first;
      this.last.item = this.second.item;
      this.cards.forEach((c) => c.index--);
      first.index = 6;
    }

    // Wait until animations are complete and then reset flag
    setTimeout(() => (this.isMoving = false), 400);
  }

  /**
   * Calculate whether a card at a given index is at the center
   * (and thus will be styled as the center card)
   * @param index The index of the card
   */
  public isCenter(index) {
    return index > 2.5 && index < 3.5;
  }

  /**
   * Mouse down event handler. Set a flag and drag-related parameters.
   * @param event The mouse event
   */
  public mousedown(event: MouseEvent) {
    this.isDragging = true;
    this.currentX = this.startX = event.pageX;
    this.draw();
  }

  /**
   * Mouse up event handler. Reset a flag and update cards.
   * @param event The mouse event
   */
  mouseup() {
    this.isDragging = false;

    this.cards.forEach((c) => (c.index = Math.round(c.index)));
  }

  /**
   * Mouse move event handler. Update drag-related parameters
   * @param event The mouse event
   */
  public mousemove(event: MouseEvent) {
    if (!this.isDragging) {
      return;
    }
    event.preventDefault();
    this.currentX = event.pageX;
  }

  /**
   * ngFor trackby function, used to track cards to prevent extraneous DOM re-rendering
   * @param _ Index - not used
   * @param card The card in question
   */
  public id(_, card: Card) {
    return card.id;
  }

  /**
   * Calculate whether a card should be hidden or not
   * @param index The index of the card in question
   */
  public getOpacity(index: number) {
    // Hide placeholder cards
    if (index < 0.2 || index > 5.8) {
      return 0;
    }

    return 1;
  }

  get first(): Card {
    return [...this.cards].sort((a, b) => a.index - b.index)[0];
  }

  get second(): Card {
    return [...this.cards].sort((a, b) => a.index - b.index)[1];
  }

  get last(): Card {
    return [...this.cards].sort((a, b) => a.index - b.index)[
      this.cards.length - 1
    ];
  }

  get secondLast(): Card {
    return [...this.cards].sort((a, b) => a.index - b.index)[
      this.cards.length - 2
    ];
  }
}
