import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public items = [
    { title: 'Mobile Internet', image: 'bench.png' },
    { title: 'Home Internet', image: 'glide.png' },
    { title: 'Get a device', image: 'bike.png' },
    { title: 'Add a Phone-line', image: 'gym-wall.png' },
    { title: 'Upgrade', image: 'snowmobile.png' },
  ];
}
