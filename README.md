# MTN Technical Test - Angular Carousel

This repository contains the code for the MTN technical test, which was to develop a responsive 5-card draggable carousel. This repository contains two codebases:

- The actual carousel component, which is published as [@jasonvh/carousel](https://www.npmjs.com/package/@jasonvh/carousel).
- A demo application, which can be seen at [carousel.vanhattum.xyz](https://carousel.vanhattum.xyz).

The following functional requirements were identified based on the specification:

- The carousel should support 5 cards - no less, no more.
- The carousel should be created as an Angular component, where the items can be dynamically changed.
- The carousel should be responsive at 3 breakpoints: 1080p, 1366p and 1920p.
- The carousel should be draggable.
- The carousel should effectively be infinite.
- There should be a 'next' and a 'back' button.
- The styling should match that of the provided design.
- No external libraries/components should be used.

It's also expected that the carousel be performant on lower-end devices.

## Future work

Had I more time, I would've liked to implement/improve the following:

- Support mobile gestures to drag the carousel.
- Improve the animations.
- Support any content in the carousel (via `<ng-content>`).
- Support any number of items.
- Use some sort of state management library (like RxJS or Akita).
- Improve mobile responsiveness.
- Fixed the known issues listed below.

## Known issues

- On the smallest breakpoint (1080p) there is extra space on the right side.
- There is a small area between the next and back buttons where the carousel isn't draggable.

## Usage

### Running the test application

`ng serve carousel-test-application`

### Building the carousel component

`ng build carousel --prod`

### Deploying the application

`ng build carousel-test-application --prod && firebase deploy`
